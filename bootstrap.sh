#!/bin/bash
# Init
FILE="/tmp/out.$$"
GREP="/bin/grep"

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
else
    function isinstalled {
      if yum list installed "$@" >/dev/null 2>&1; then
        return 1
      else
        return 0
      fi
    }
    
    #echo "Checking for and installing any updates."
    #yum -y update
    
    if [ ! -f /etc/yum.repos.d/nginx.repo ]; then
        echo "Installing the repository for NGINX Server"
        yum -y install http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
    fi
    
    if [ ! -f /etc/yum.repos.d/ius.repo ]; then
        echo "Installing the repository for IUS Community Project"
        yum -y install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
    fi
    
    if [ ! -f /etc/yum.repos.d/percona-release.repo ]; then
        echo "Installing the repository for Percona MySQL Server"
        yum -y install http://www.percona.com/downloads/percona-release/redhat/0.1-3/percona-release-0.1-3.noarch.rpm
    fi
    
    mkdir -p /vagrant/.logs/
    
    if [ ! -f /vagrant/.logs/nginx ]; then
        echo "Installing NGINX Server"
        yum -y install nginx
        touch /vagrant/.logs/nginx
    fi
    
    if [ ! -f /vagrant/.logs/php ]; then
        echo "Installing PHP 5.6"
        yum -y install php56u
        yum -y install php56u-cli
        yum -y install php56u-fpm
        yum -y install php56u-mysqlnd
        yum -y install php56u-ldap
        
        sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php.ini
        sed -i "s/display_errors = .*/display_errors = On/" /etc/php.ini
	
	touch /vagrant/.logs/php
    fi
    
    if [ ! -f /vagrant/.logs/percona ]; then
        echo "Installing Percona MySQL Server"
        yum -y install Percona-Server-server-56
        
	echo "CREATE USER 'vagrant'@'%' IDENTIFIED BY 'password'" | mysql -uroot
        echo "GRANT ALL ON *.* TO 'vagrant'@'%'" | mysql -uroot
        
	touch /vagrant/.logs/percona
    fi
    
    mkdir -p /vagrant/www
    cd /vagrant
    mv im*/.info.php www/
    mv im*/.404.html www/
    mv im*/.403.html www/
    mv im*/.50x.html www/
    
    rm /etc/my.cnf
    mv im*/my.cnf /etc/
    
    rm /etc/nginx/nginx.conf
    mv im*/nginx.conf /etc/nginx/nginx.conf
    
    rm -R /vagrant/im*
    rm vagrant.zip
    
    systemctl stop nginx
    systemctl stop php-fpm
    systemctl stop mysql
    systemctl start nginx
    systemctl start php-fpm
    systemctl start mysql
fi